﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;

using LanguageExt;

namespace Nexogen.Libraries.Blockchain.Schema
{
    public sealed class Block : Record<Block>
    {
        [Pure]
        public static Try<Block> CreateBlock(long index, string currHash, string prevHash, DateTime timestamp, string data) => () =>
            new Block(index, currHash, prevHash, timestamp, data);

        public long Index { get; }

        public string CurrHash { get; }

        public string PrevHash { get; }

        public DateTime Timestamp { get; }

        public string Data { get; }

        private Block(long index, string currHash, string prevHash, DateTime timestamp, string data)
        {
            Index = index;
            CurrHash = !string.IsNullOrEmpty(currHash) && IsValidHash(currHash) ? currHash : throw new ArgumentException(nameof(currHash));
            PrevHash = string.IsNullOrEmpty(prevHash) || IsValidHash(prevHash) ? prevHash : throw new ArgumentException(nameof(prevHash));
            Timestamp = timestamp;
            Data = data;
        }

        [Pure]
        internal static bool IsValidHash(string hashValue) =>
            !string.IsNullOrEmpty(hashValue) &&
            (hashValue.Length == 64 || hashValue.Length == 128) &&
            hashValue.All(c => (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'));
    }
}
