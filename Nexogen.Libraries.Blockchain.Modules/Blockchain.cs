﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;

using LanguageExt;
using Nexogen.Libraries.Blockchain.Schema;

using static LanguageExt.Prelude;
using static Nexogen.Libraries.Blockchain.Schema.Block;
using static Nexogen.Libraries.Blockchain.Modules.Hashing;

namespace Nexogen.Libraries.Blockchain.Modules
{
    public static class Blockchain
    {
        #region Modify these in your fork to create +1B USD "additional value" on the market. ;)

        public const string HashName = "SHA-256";
        public const string GenesisBlockHash = "59b0e255dcca558e28eed988258c92c8b151fa3bca8047be1a323415e939fc23";
        public const string GenesisBlockData = "NEXOGEN Blockchain Genesis Block";
        public static readonly DateTime GenesisBlockTime = new DateTime(636521503200000000, DateTimeKind.Utc);

        #endregion

        [Pure]
        private static Try<Func<string, string>> HashAlgorithm { get; }
            = CreateHashAlgorithm(HashName);

        [Pure]
        internal static Try<string> BlockHash(long index, string prevHash, DateTime timestamp, string data) =>
            from hashAlgorithm in HashAlgorithm
            select hashAlgorithm($"{index}{prevHash}{timestamp.Ticks}{data}");
        
        [Pure]
        internal static Option<string> BlockHash(Option<Block> block) =>
            from thisBlock in block
            from blockHash in BlockHash(thisBlock.Index, thisBlock.PrevHash, thisBlock.Timestamp, thisBlock.Data).ToOption()
            select blockHash;
        
        [Pure]
        internal static Option<Block> GenesisBlock { get; } =
            CreateBlock(0, GenesisBlockHash, null, GenesisBlockTime, GenesisBlockData).ToOption();

        [Pure]
        internal static bool IsValidGenesisBlock(Option<Block> block) =>
            ifNone (from genesisBlock in GenesisBlock
                    from thisBlock in block
                    select thisBlock == genesisBlock,
                    () => false);

        [Pure]
        internal static bool IsValidBlock(Option<Block> nextBlock, Option<Block> prevBlock) =>
            ifNone (from nextBlk in nextBlock
                    from prevBlk in prevBlock
                    select (prevBlk.Index + 1 == nextBlk.Index) &&
                           (prevBlk.CurrHash == nextBlk.PrevHash) &&
                           (nextBlk.CurrHash == BlockHash(nextBlock)),
                    () => false);

        [Pure]
        internal static bool IsValidBlockchain(Lst<Block> blockchain) =>
            IsValidGenesisBlock(blockchain.FirstOrDefault()) &&
            blockchain.Skip(1)
                      .Zip(blockchain, (nextBlock, prevBlock) => (nextBlock, prevBlock))
                      .All(pair => IsValidBlock(Some(pair.nextBlock), Some(pair.prevBlock)));

        [Pure]
        internal static Option<Block> NextBlock(Lst<Block> blockchain, DateTime timestamp, string data) =>
            from lastBlock in Optional(blockchain.LastOrDefault())
            let nextHash = BlockHash(lastBlock.Index + 1, lastBlock.CurrHash, timestamp, data)
            from nextBlock in CreateBlock(lastBlock.Index + 1, ifFail(nextHash, () => null), lastBlock.CurrHash, timestamp, data).ToOption()
            select nextBlock;

        [Pure]
        public static Option<Lst<Block>> CreateBlockchain() =>
            from genesisBlock in GenesisBlock
            select List(genesisBlock);
        
        [Pure]
        public static (Lst<Block> Blockchain, Option<Block> AppendedBlock) AppendToBlockchainIfValid(Lst<Block> blockchain, Option<Block> block) =>
            IsValidBlock(block, blockchain.LastOrDefault())
            ? block.Some(nb => (blockchain.Add(nb), block))
                   .None(() => (blockchain, None))
            : (blockchain, None);

        // TODO: broadcast latest block in free I/O monad iff AppendedBlock != None.
        [Pure]
        public static (Lst<Block> Blockchain, Option<Block> AppendedBlock) AppendToBlockchain(Lst<Block> blockchain, DateTime timestamp, string data) =>
            AppendToBlockchainIfValid(blockchain, NextBlock(blockchain, timestamp, data));

        // TODO: broadcast latest block in free I/O monad iff IsChainReplaced == true.
        [Pure]
        public static (Lst<Block> Blockchain, bool IsChainReplaced) SelectNewBlockchainIfValid(Lst<Block> oldBlockchain, Lst<Block> newBlockchain) =>
            IsValidBlockchain(newBlockchain) && newBlockchain.Count > oldBlockchain.Count ? (newBlockchain, true) : (oldBlockchain, false);
    }
}
