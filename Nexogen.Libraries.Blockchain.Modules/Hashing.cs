﻿using System;
using System.Diagnostics.Contracts;
using System.Security.Cryptography;

using LanguageExt;

using static System.Text.Encoding;
using static LanguageExt.Prelude;

namespace Nexogen.Libraries.Blockchain.Modules
{
    internal static class Hashing
    {
        private static readonly Func<HashAlgorithm, string, string> hashFunction = (hashAlgorithm, data) =>
            hashAlgorithm.ComputeHash(UTF8.GetBytes(data))
                .Fold(string.Empty, (str, b) => str + b.ToString("x2"));

        //
        // NOTE:
        // https://github.com/dotnet/corefx/issues/22626
        // var hash = HashAlgorithm.Create(hashName);
        [Pure]
        internal static Try<Func<string, string>> CreateHashAlgorithm(string hashName) => () =>
            par(hashFunction, (HashAlgorithm)CryptoConfig.CreateFromName(hashName) ?? throw new ArgumentException(hashName));
    }
}
