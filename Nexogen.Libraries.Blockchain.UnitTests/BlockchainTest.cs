﻿using System;

using FluentAssertions;
using LanguageExt;
using Xunit;

using Nexogen.Libraries.Blockchain.Schema;

using static LanguageExt.Prelude;
using static Nexogen.Libraries.Blockchain.Schema.Block;
using static Nexogen.Libraries.Blockchain.Modules.Blockchain;

namespace Nexogen.Libraries.Blockchain.UnitTests
{
    public sealed class BlockchainTest
    {
        private const long BlockIndex = 155L;
        private const string BlockCurrHash = "a82d04a6cccc3a00560c7bc901e2797e6d38ee51b806d50af464a6f2597a2fd7";
        private const string BlockPrevHash = "df4d8ddb4b230bc452655e37b52130e90f13830a44a4466bd513fab0a4bdf68e";
        private readonly static DateTime BlockTimeStamp = new DateTime(2018, 1, 21);
        private const string BlockData = "Hello Blockchain!";

        private static Action<object> ShouldFail() => p => false.Should().BeTrue();

        [Fact]
        public void TestBlockHashOfParams() =>
            BlockHash(BlockIndex, BlockPrevHash, BlockTimeStamp, BlockData)
                .Map(blockHash => blockHash)
                .ToOption()
                .Equals(Some(BlockCurrHash))
                .Should().BeTrue();

        [Fact]
        public void TestBlockHashOfNone() =>
            BlockHash(None)
                .Map(blockHash => unit)
                .Equals(None)
                .Should().BeTrue();

        Option<bool> BlockHashIsEquivalentToCurrHash(Block block) =>
            BlockHash(Some(block))
                .Map(blockHash => blockHash == BlockCurrHash);

        [Fact]
        public void TestBlockHashOfIBlock() =>
            CreateBlock(BlockIndex, BlockCurrHash, BlockPrevHash, BlockTimeStamp, BlockData)
                .ToOption()
                .Bind(BlockHashIsEquivalentToCurrHash)
                .Equals(Some(true))
                .Should().BeTrue();

        [Fact]
        public void TestGenesisBlockIsNotNone() =>
            GenesisBlock
                .Map(genesisBlock => true)
                .Equals(Some(true))
                .Should().BeTrue();

        [Fact]
        public void TestGenesisBlockEquality() =>
            GenesisBlock
                .Bind(genesisBlock =>
                        CreateBlock(0, GenesisBlockHash, null, GenesisBlockTime, GenesisBlockData)
                            .Map(currBlock => genesisBlock == currBlock).ToOption())
                .Equals(Some(true))
                .Should().BeTrue();


        [Fact]
        public void TestNoneIsNotValidGenesisBlock() =>
            IsValidGenesisBlock(None)
                .Should().BeFalse();

        [Fact]
        public void TestGenesisBlockIsValidGenesisBlock() =>
            IsValidGenesisBlock(GenesisBlock)
                .Should().BeTrue();

        [Fact]
        public void TestIsValidBlockNextAndGenesis() =>
            BlockHash(1, GenesisBlockHash, BlockTimeStamp, BlockData)
                .Bind(currHash =>
                        CreateBlock(1, currHash, GenesisBlockHash, BlockTimeStamp, BlockData)
                            .Map(nextBlock => IsValidBlock(Some(nextBlock), GenesisBlock)))
                .ToOption()
                .Equals(Some(true))
                .Should().BeTrue();

        [Fact]
        public void TestIsValidBlockNextIsNoneFails() =>
            IsValidBlock(None, GenesisBlock)
                .Should().BeFalse();

        [Fact]
        public void TestIsValidBlockPrevIsNoneFails() =>
            IsValidBlock(GenesisBlock, None)
                .Should().BeFalse();

        [Fact]
        public void TestEmptyChainIsInvalid() =>
            IsValidBlockchain(List<Block>())
                .Should().BeFalse();

        [Fact]
        public void TestOneItemChainWithGenesisBlockIsValid() =>
            GenesisBlock
                .Map(genesisBlock => IsValidBlockchain(List(genesisBlock)))
                .Equals(Some(true))
                .Should().BeTrue();

        [Fact]
        public void TestOneItemChainWithAnyNonGenesisBlockIsInvalid() =>
            CreateBlock(1, BlockCurrHash, BlockPrevHash, BlockTimeStamp, BlockData)
                .Map(currBlock => IsValidBlockchain(List(currBlock)))
                .ToOption()
                .Equals(Some(false))
                .Should().BeTrue();

        [Fact]
        public void TestChainOfTwoItemsFirstIsGenesisSecondIsAValidNextBlockIsValid() =>
            GenesisBlock
                .Bind(genesisBlock =>
                    BlockHash(genesisBlock.Index + 1, genesisBlock.CurrHash, BlockTimeStamp, BlockData)
                        .Bind(currHash =>
                            CreateBlock(genesisBlock.Index + 1, currHash, genesisBlock.CurrHash, BlockTimeStamp, BlockData)
                                .Map(nextBlock =>
                                    IsValidBlockchain(List(genesisBlock, nextBlock)))).ToOption())
                        .Equals(Some(true))
                        .Should().BeTrue();

        [Fact]
        public void TestChainOfTwoItemsFirstIsGenesisSecondIsAnInvalidNextBlockIsInvalid() =>
            GenesisBlock
                .Bind(genesisBlock =>
                    BlockHash(genesisBlock.Index + 1, genesisBlock.CurrHash, BlockTimeStamp, BlockData)
                        .Bind(currHash =>
                            CreateBlock(genesisBlock.Index + 1, currHash, BlockPrevHash, BlockTimeStamp, BlockData)
                                .Map(nextBlock =>
                                    IsValidBlockchain(List(genesisBlock, nextBlock)))).ToOption())
                        .Equals(Some(false))
                        .Should().BeTrue();

        [Fact]
        public void TestNextBlockIsNoneIfChainIsEmpty() =>
            NextBlock(List<Block>(), BlockTimeStamp, BlockData)
                .Map(nextBlock => unit)
                .Equals(None)
                .Should().BeTrue();

        [Fact]
        public void TestNextBlockIsValid() =>
            GenesisBlock
                .Bind(genesisBlock =>
                    BlockHash(genesisBlock.Index + 1, genesisBlock.CurrHash, BlockTimeStamp, BlockData)
                        .Bind(currHash =>
                            CreateBlock(genesisBlock.Index + 1, currHash, genesisBlock.CurrHash, BlockTimeStamp, BlockData)
                                .Map(currBlock =>
                                    NextBlock(List(genesisBlock), BlockTimeStamp, BlockData)
                                        .Map(nextBlock =>
                                            currBlock.Equals(nextBlock))
                                        .ToOption()))
                        .ToOption())
                .Equals(Some(true))
                .Should().BeTrue();

        [Fact]
        public void TestCreateBlockchain() =>
            CreateBlockchain()
                .Map(blockchain => IsValidBlockchain(blockchain))
                .Equals(Some(true))
                .Should().BeTrue();

        [Fact]
        public void TestAppendValidBlockToEmptyBlockchainIsInvalid() =>
            AppendToBlockchainIfValid(List<Block>(), GenesisBlock)
                .Equals((List<Block>(), Optional(default(Block))))
                .Should().BeTrue();

        [Fact]
        public void TestAppendNoneToValidBlockchainIsInvalid() =>
            CreateBlockchain()
                .Map(blockchain =>
                        AppendToBlockchainIfValid(blockchain, None)
                            .Equals((blockchain, Optional(default(Block)))))
                .Equals(Some(true))
                .Should().BeTrue();

        [Fact]
        public void TestAppendInvalidBlockToValidBlockchainIsInvalid() =>
            CreateBlockchain()
                .Bind(blockchain =>
                        CreateBlock(BlockIndex, BlockCurrHash, BlockPrevHash, BlockTimeStamp, BlockData)
                            .Map(newBlock => AppendToBlockchainIfValid(blockchain, Some(newBlock))
                                                .Equals((blockchain, Optional(default(Block)))))
                            .ToOption())
                .Equals(Some(true))
                .Should().BeTrue();

        [Fact]
        public void TestAppendValidBlockToValidBlockchainIsValid() =>
            CreateBlockchain()
                .Bind(blockchain =>
                        NextBlock(blockchain, BlockTimeStamp, BlockData)
                            .Map(newBlock => AppendToBlockchainIfValid(blockchain, Some(newBlock))
                                                .Equals((blockchain.Add(newBlock), Some(newBlock))))
                            .ToOption())
                .Equals(Some(true))
                .Should().BeTrue();

        [Fact]
        public void TestAppendToEmptyBlockchainIsInvalid() =>
            AppendToBlockchain(List<Block>(), BlockTimeStamp, BlockData)
                .Should()
                .BeEquivalentTo((List<Block>(), Optional(default(Block))));

        [Fact]
        public void TestAppendToValidBlockchainIsValid() =>
            CreateBlockchain()
                .Bind(blockchain => 
                        NextBlock(blockchain, BlockTimeStamp, BlockData)
                            .Map(nextBlock => AppendToBlockchain(blockchain, BlockTimeStamp, BlockData)
                                                .Equals((blockchain.Add(nextBlock), Some(nextBlock))))
                            .ToOption())
                .Equals(Some(true))
                .Should().BeTrue();

        [Fact]
        public void TestSelectNewBlockchainIfValidBothEmpty() =>
            SelectNewBlockchainIfValid(List<Block>(), List<Block>())
                .Should().BeEquivalentTo((List<Block>(), false));

        [Fact]
        public void TestSelectNewBlockchainIfValidNewIsEmptyOldIsValid() =>
            CreateBlockchain()
                .Map(blockchain =>
                        SelectNewBlockchainIfValid(blockchain, List<Block>())
                            .Equals((blockchain, false)))
                .Equals(Some(true))
                .Should().BeTrue();

        [Fact]
        public void TestSelectNewBlockchainIfValidOldIsEmptyNewIsValid() =>
            CreateBlockchain()
                .Map(blockchain =>
                        SelectNewBlockchainIfValid(List<Block>(), blockchain)
                            .Equals((blockchain, true)))
                .Equals(Some(true))
                .Should().BeTrue();

        [Fact]
        public void TestSelectNewBlockchainIfValidOldIsValidNewIsValidAndLonger() =>
            CreateBlockchain()
                .Map(blockchain =>
                        AppendToBlockchain(blockchain, BlockTimeStamp, BlockData)
                            .Map(results =>
                                SelectNewBlockchainIfValid(blockchain, results.Item1)
                                    .Equals((results.Item1, true))))
                .Equals(Some(true))
                .Should().BeTrue();
    }
}
