using System;

using FluentAssertions;
using Xunit;

using static Nexogen.Libraries.Blockchain.Schema.Block;

namespace Nexogen.Libraries.Blockchain.UnitTests
{
    public sealed class BlockTest
    {
        private const long BlockIndex = 155L;
        private const string BlockCurrHash = "a82d04a6cccc3a00560c7bc901e2797e6d38ee51b806d50af464a6f2597a2fd7";
        private const string BlockPrevHash = "df4d8ddb4b230bc452655e37b52130e90f13830a44a4466bd513fab0a4bdf68e";
        private readonly static DateTime BlockTimeStamp = new DateTime(2018, 1, 21);
        private const string BlockData = "Hello Blockchain!";

        [Fact]
        public void TestCreateBlock() =>
            CreateBlock(BlockIndex, BlockCurrHash, BlockPrevHash, BlockTimeStamp, BlockData)
                .Map(block =>
                    (block.Index == BlockIndex) &&
                    (block.CurrHash == BlockCurrHash) &&
                    (block.PrevHash == BlockPrevHash) &&
                    (block.Timestamp == BlockTimeStamp) &&
                    (block.Data == BlockData))
                .IfFail(false)
                .Should().BeTrue();

        [Fact]
        public void TestCreateBlockThrowsExceptionIfCurrHashIsNull() =>
            CreateBlock(BlockIndex, null, BlockPrevHash, BlockTimeStamp, BlockData)
                .Map(block => false)
                .IfFail(true)
                .Should().BeTrue();

        [Fact]
        public void TestCreateBlockPrevHashCanBeNull() =>
            CreateBlock(BlockIndex, BlockCurrHash, null, BlockTimeStamp, BlockData)
                .Map(block =>
                    (block.Index == BlockIndex) &&
                    (block.CurrHash == BlockCurrHash) &&
                    (block.PrevHash == null) &&
                    (block.Timestamp == BlockTimeStamp) &&
                    (block.Data == BlockData))
                .IfFail(false)
                .Should().BeTrue();

        [Fact]
        public void TestCreateBlockDataCanBeNull() =>
            CreateBlock(BlockIndex, BlockCurrHash, BlockPrevHash, BlockTimeStamp, null)
                .Map(block =>
                    (block.Index == BlockIndex) &&
                    (block.CurrHash == BlockCurrHash) &&
                    (block.PrevHash == BlockPrevHash) &&
                    (block.Timestamp == BlockTimeStamp) &&
                    (block.Data == null))
                .IfFail(false)
                .Should().BeTrue();

        [Fact]
        public void TestBlockPairEquality() =>
            CreateBlock(BlockIndex, BlockCurrHash, BlockPrevHash, BlockTimeStamp, BlockData)
                .Map(block1 =>
                        CreateBlock(BlockIndex, BlockCurrHash, BlockPrevHash, BlockTimeStamp, BlockData)
                            .Map(block2 => block1.Equals(block2))
                            .IfFail(false))
                .IfFail(false)
                .Should().BeTrue();

        [Fact]
        public void TestBlockPairNonEquality() =>
            CreateBlock(BlockIndex, BlockCurrHash, BlockPrevHash, BlockTimeStamp, BlockData + "1")
                .Map(block1 =>
                        CreateBlock(BlockIndex, BlockCurrHash, BlockPrevHash, BlockTimeStamp, BlockData)
                            .Map(block2 => block1.Equals(block2))
                            .IfFail(true))
                .IfFail(true)
                .Should().BeFalse();

        [Fact]
        public void TestHashingNullIsNotValidHash() =>
            IsValidHash(null)
                .Should().BeFalse();

        [Fact]
        public void TestHashingEmptyStringIsNotValidHash() =>
            IsValidHash(string.Empty)
                .Should().BeFalse();

        [Fact]
        public void TestHashingLength64IsValid() =>
            IsValidHash(BlockCurrHash)
                .Should().BeTrue();

        [Fact]
        public void TestHashingLength128IsValid() =>
            IsValidHash(BlockCurrHash + BlockPrevHash)
                .Should().BeTrue();

        [Fact]
        public void TestHashingLengthOtherIsInvalid() =>
            IsValidHash(BlockCurrHash.Substring(0, BlockCurrHash.Length - 1))
                .Should().BeFalse();

        [Fact]
        public void TestHashingUppercaseIsInvalid() =>
            IsValidHash(BlockCurrHash.ToUpperInvariant())
                .Should().BeFalse();
    }
}
