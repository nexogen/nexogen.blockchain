﻿using FluentAssertions;
using System;
using Xunit;

using static LanguageExt.Prelude;
using static Nexogen.Libraries.Blockchain.Modules.Hashing;

namespace Nexogen.Libraries.Blockchain.UnitTests
{
    public sealed class HashingTest
    {
        private const string SHA256 = "SHA-256";
        private const string HelloWorld = "Hello World!!!";
        private const string HelloWorldSHA256Hash = "073f7397b078dca7efc7f9dc05b528af1afbf415d3caa8a5041d1a4e5369e0b3";

        private static Action<object> ShouldSucceed() => p => true.Should().BeTrue();
        private static Action<object> ShouldFail() => p => false.Should().BeTrue();

        [Fact]
        public void TestHashingNullHashName() =>
            CreateHashAlgorithm(null)
                .Succ(ShouldFail())
                .Fail(ShouldSucceed());

        [Fact]
        public void TestHashingEmptyHashName() => 
            CreateHashAlgorithm(string.Empty)
                .Succ(ShouldFail())
                .Fail(ShouldSucceed());

        [Fact]
        public void TestHashingCreateSHA256() =>
            CreateHashAlgorithm(SHA256)
                .Succ(ShouldSucceed())
                .Fail(ShouldFail());

        [Fact]
        public void TestHashingSHA256HashValue() =>
            CreateHashAlgorithm(SHA256)
                .Map(h => h(HelloWorld))
                .ToOption()
                .Equals(Some(HelloWorldSHA256Hash))
                .Should().BeTrue();
    }
}
