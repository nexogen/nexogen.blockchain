﻿using System;

using LanguageExt;
using Proto;

namespace Nexogen.Libraries.Blockchain
{
    using System.Threading.Tasks;
    using static Hello;

    internal sealed class Hello : Record<Hello>
    {
        public readonly string Who;

        public static Hello NewHello(string who)
            => new Hello(who);

        public Hello(string who)
            => Who = who;
    }

    static class Program
    {
        async static Task Main(string[] args)
        {
            var props = Actor.FromFunc(ctx => 
            {
                if (ctx.Message is Hello r)
                {
                    ctx.Respond($"Hello {r.Who}");
                }

                return Actor.Done;
            });

            var pid = Actor.Spawn(props);
            var response = await pid.RequestAsync<string>(NewHello("Alex"));

            Console.WriteLine(response);

            //pid.Tell(new Hello
            //{
            //    Who = "Alex"
            //});
        }
    }
}
